from website import create_app 
'''
Commande disponnible car le dossier website est considéré comme un package python 
Grâce à la présence du fichier __init__.py
'''

app = create_app()

if __name__ == '__main__':
    app.run(debug=True)