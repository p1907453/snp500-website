'''
Fichier le plus important du projet. Gère le développement serveur des différentes pages du projet, ainsi que la connexion à la Base de Données.
'''
from flask import Blueprint, render_template, request #fonctions utiles de flask pour le backend.
from . import sql #variable de connexion à la base de données définie dans __init__.py


views = Blueprint('views', __name__) #Pour définir les routes dans l'application
stockList = sql.request("select distinct name from allstocks;")
newList = [item[0] for item in stockList ] #liste des actions au bon format



@views.route('/', methods=['GET', 'POST']) #création du chemin / dans la page web
def home():#fonction de la route / 
    if request.method == 'POST':
        stock = request.form.get('stock') #Récupération des données du formulaire pour faire les requetes en conséquence
        dataType = request.form.get('datatype')
        query = "SELECT date, " + dataType + " from allstocks where name like '" + stock + "' order by date;" #requête à effectuer
        result = sql.request(query)
        labels = [res[0].date() for res in result] #liste de dates 
        values = [res[1] for res in result]#liste des données
        return render_template("bourse.html", labels = labels, values = values, liste= newList, stock = stock, dataType = dataType) #affichage du html python

    return render_template("bourse.html",liste = newList)#affichage du html python

@views.route('/stats', methods=['GET', 'POST'])#création du chemin / dans la page web
def stats():
    if request.method == 'POST':
        stock = request.form.get('stock') #Nom de l'action choisie dans le formulaire        
        #Suite de requêtes avec récupération du resultat dans des variables.
        query = "select date, max(high) from allstocks where name like '"+ stock +"' and high = (select max(high) from allstocks where name like '"+ stock +"');"
        result = sql.request(query)
        print(result[0][0])
        maxDate = result[0][0].date()
        max = result[0][1]
        query = "select date, min(low) from allstocks where name like '"+ stock +"' and low = (select min(low) from allstocks where name like '"+ stock +"');"
        result = sql.request(query)
        minDate = result[0][0].date()
        min = result[0][1]
        query = "select avg(open) from allstocks where name like '"+ stock + "';"
        result = sql.request(query)
        avg = result[0][0]
        query = "select max(open)-min(open) from allstocks where name like '"+ stock +"';"
        result = sql.request(query)
        ecartType = result[0][0]
        query = "select A.open-B.open from allstocks A join allstocks B on A.name like '"+ stock + "' and B.name like '"+ stock + "' and datediff(A.date,(select max(date) from allstocks where name like '"+ stock + "' ))=0 and datediff(B.date,(select min(date) from allstocks where name like '"+ stock +"' ))=0"
        result = sql.request(query)
        Difference = result[0][0]
        return render_template("statistiques.html", liste = newList, stock = stock , maxDate = maxDate, max = max, minDate = minDate, min = min, avg = avg, ecartType = ecartType, Difference = Difference)


    return render_template("statistiques.html", liste = newList)#affichage du html en python avec les paramètres passés

