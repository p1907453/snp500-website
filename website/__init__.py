from flask import Flask
import mysql.connector as MC
#Classe de connexion à mysql : Pour faciliter les requêtes
class requetesSQL:
    def __init__(self, host, user, database):
        try:
            self.conn = MC.connect(host = host, user = user, database = database)
        except MC.errors as err:
            print(err)
    
    def request(self, query):#retourne les résultats de la requête sous forme de tuples
        cursor = self.conn.cursor() #Pour les requetes
        cursor.execute(query) #execution de la requete
        result = cursor.fetchall() #homogénisation du résultat
        cursor.close() #fermeture du curseur de requetes
        return result


#Fichier clé du projet. Permet de rendre website en package. S'execute avant tout le reste. 

sql = requetesSQL("localhost", "root", 'snp500')#instance de la classe requetesSQL pour faciliter les requetes.

def create_app():
    #init app
    app = Flask(__name__)
    app.config['SECRET_KEY'] = ' '

    from .views import views
    app.register_blueprint(views, url_prefix='/') #on enregistre les vues crées dans views

    return app
