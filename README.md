# SNP500-Website

Ce projet est un site internet permettant de traiter les données de l'actif boursier S&P 500.
Cette base de données est disponible à l'adresse suivante : https://www.kaggle.com/camnugent/sandp500
Elle contient les informations des prix à l'ouverture, à la fermeture, les prix maximaux et minimaux atteints ainsi que le volume d'échange pour chaque titre du S&P 500 pour chaque jour entre 2013 et 2018 (à verifier).
Le but de ce projet et de réaliser un site permettant d'extraire de la base les données souhaitées par l'utilisateur et de les afficher convenablement.

Comment initialiser la base de données : 
    1 : Télecharger MySQL
    2 : Créer l'utilisateur root avec les privilèges max dans MySQL Workbench et sans mot de passe.
    3 : Ajouter MySQLServer/bin dans les variables d'environnement
    4 : Créer la base de données :
            A) Avoir le fichier "all_stocks_5yr.csv" (disponible à la racine de ce git)
            B) Lancer MySQL avec Local-infile = 1 pour pouvoir importer les données d'un .csv dans la base de données
                $ mysql --local-infile=1 -u root -p
            C) Créer la base et les données:
                 Une fois connecté à MySQL en ligne de commande : 
                    CREATE DATABASE snp500;
                    CREATE TABLE allstocks (`date` datetime, `open` double, `high` double, `low` double, `close` double, `volume` int, `Name` text) ENGINE = InnoDB
                    LOAD DATA LOCAL INFILE 'path/to/data.csv' INTO TABLE allstocks FIELDS TERMINATED BY "," LINES TERMINATED BY "\n" IGNORE 1 LINES (date,open,high,low,close,volume,Name).

                ('path/to/data.csv' est le chemin absolu vers le fichier "all_stocks_5yr.csv" )

Installer les librairies python nécessaires : 
    pip install flask
    pip install mysql-connector
    pip install mysql-connector-python

Pour lancer le site web : 
    Executer le fichier main.py.
    Sur un navigateur, aller à l'adresse 127.0.0.1:5000
    Le site internet est affiché et opérationnel.

Pour quitter le site :
    Mettre fin à l'execution du programme main.py (ctrl+c dans le terminal).

Arborescence du projet : 
    Racine du projet : 
        -allstocks.csv : les données en format csv
        -Readme.md : ce fichier !
        -main.py : le fichier à executer
        /website:
            /static:
                /images:
                    Les différentes images du projet
                /css:
                    Les 2 feuilles de style des pages html
            /templates:
                Les 2 pages html modulables avec python grace à flask avec l'utilisation des {{}}

Nous utilisons la programmation objet afin de faciliter les requêtes sql, avec la classe requetesSQL définie dans __init__.py

Ce projet a été réalisé par le groupe 2 de la promotion 3A Informatique 2021-2022 de Polytech Lyon.
